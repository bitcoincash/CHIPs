# CHIP 2021-01 Restrict Transaction Version

## Summary

This proposal prevents transactions from being accepted and mined if they provide an invalid version number.

> Title: Restrict Transaction Version  
> Date: 2021-01  
> Type: Consensus  
> Version: 0.9  
> Last edit: 2022-01-14  
> Owner: Tom Zander, Jonathan Silverblood  
> History: https://gitlab.com/bitcoin.cash/chips  
> Discussion: [bitcoincashresearch](https://bitcoincashresearch.org/t/173)

## Motivation and Benefits

Currently, the transaction version field is not enforced as a consensus rule and current full node software does not make a distinction in how transactions are parsed based on the transaction version field.

If in the future we would introduce a new transaction format it would make sense to use the version field for its intended purpose, and use it to determine how to parse transactions.

However, if we do not enforce valid transaction version numbers it would be possible for external actors to repurpose the field for their own usage and a future upgrade to reject invalid version numbers would then break existing usage.

By enforcing valid transaction versions now we can prevent this situation, and reduce complexity for future upgrades to transaction versions.


## Impact / Costs and Risks

The overall risks and costs of this proposal are very small, as the change only impact developers who write block validation code and users (mostly node operators) who run such code.

All other participants in the ecosystem are unaffected by this change.

Failure to implement this update could result in external actors repurposing the version field for their own uses, making future upgrades more complex.

### Developers

Developers of software that validates blocks will need to implement a minor change to reject transactions and blocks containing invalid version numbers.

### Node operators, etc.

Users who run block validation software need to update to a supporting version in a timely manner, or they could find themselves out of consensus with the network.

*Note: Since this proposal is intended to activate at future upgrade point, this cost is considered negliable and is shared with all other proposals being activated in the same upgrade.*


### Historical usage

After inspecting the current blockchain we can conclude that in total there has only been six (6) transactions that have presented an invalid version number, the most recent one dating back to 2016:

blockheight|hash|version
|---|---|---|
256818|637dd1a3418386a418ceeac7bb58633a904dbf127fa47bbea9cc8f86fef7413f|-2107285824
256818|c659729a7fea5071361c2c1a68551ca2bf77679b27086cc415adeeb03852e369|-1703168784
257644|35e79ee733fad376e76d16d1f10088273c2f4c2eaba1374a837378a88e530005|-2130706433
311495|64147d3d27268778c9d27aa434e8f270f96b2be859658950accde95a2f0ce79d|0
370002|110da331fd5336038316c4709404aea5855afed21f054f5bba01bfef099d5da1|3
407042|6ae17e22dba03522126f9268de58de5a440ccdb334e137861f90766901e806fd|4


## Technical Specification

All transactions must begin with a 4 byte unsigned integer in little-endian version number corresponding to a consensus-enforced transaction format.

Transactions that reference a version number that is not enforced by consensus rules on the network shall be rejected as invalid.

Blocks that include transactions that violate this rule shall also be rejected as invalid.

At the time of writing this CHIP the only valid version numbers are `1` AND `2`, and is already enforced by standardness rules.

Future improvements may introduce new transaction formats with their own unique identifiers, and at such time that they are enforced by consensus they would become valid according to this proposal.

## Implementations

[BCHN merge request](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1599).

## Stakeholders

Since standardness policies already require that transaction versions must be either 1 or 2, and on-chain analysis indicates that there are no users of non-valid transaction versions since 2016 the only clearly identifiable stakeholders are miners and developers of transaction and block validating software.

## Statements

**BigBlockIfTrue, BCHN developer.**  
> Reviewed, looks good to me. Recommend activation of this CHIP in the first network upgrade after May 2021. [source](https://bitcoincashresearch.org/t/173/7)

**Freetrader, BCHN release manager.**  
> Likewise [source](https://bitcoincashresearch.org/t/173/10)

**Andrew Stone, developer Bitcoin Unlimited**  
> I support this change, so BU does so unless a BUIP is explicitly raised against it. ([source](https://bitcoincashresearch.org/t/173/11))

**Fernando Pelliccioni, BCHN developer, Knuth lead dev.**  
> I support this change as a BCHN developer and Knuth lead dev. [source](https://bitcoincashresearch.org/t/173/31)

**Calin Culianu, BCHN Developer**
> I strongly support this as a new consensus rule moving forward for any future upgrade [source](https://bitcoincashresearch.org/t/restrict-transaction-version-numbers/173/30)

**bitcoincashautist, contributor**
> This proposal has value in that it would enable context-free transaction parsing, and would come at no cost to anyone. Finally we could use the transaction version field to actually track transaction versions and map them to Bitcoin Cash upgrades.

**Josh Green, Bitcoin Verde**
> We support restricting the transaction version field so that the consensus rules are consistent with current network rules.

**Rucknium, CashFusion Red Team**

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

I support the Restrict Transaction Version CHIP. It is a good rule to prevent
an excessive diversity of transaction version numbers that could reduce
transaction uniformity and coin fungibility.
-----BEGIN PGP SIGNATURE-----

iQJMBAEBCgA2FiEEXV4Iojid8iWr0HgbKR4MIp1jFpcFAmNuOUAYHHJ1Y2tuaXVt
QHByb3Rvbm1haWwuY29tAAoJECkeDCKdYxaX2ccQAImVeYmAGkl1U2f9n926HtFZ
x2cnSZcAGvJM6g3DrOFqZq+X2kMLviln11kYETwnmwTZF7frdrI9GdC6cpiQ+1E9
WfBUkGHtTrynJxtvvqUiD8b3mye1dYbnD0CBdX8qlBoVP9cBkRgBe/Qxw8Xk4gBP
nh/qNZUkDbSrUmArArzi71dmUA3bxDD7O/s7RaJdwBpQDE1jPWEvOuGeR/1S8gs8
nuPgQntIZBX9wLi8S09XM/ndsTokBeHfdJlLAO0WYCQWQ2fWSYpL2YXUCE1G6WBw
PcLJtMHsNKXzDW1yo64KPOsdbDT2BPU4W5Jod3ul1zGiUglOyRdgQ/PZPmzpGk6b
n/9WT6oP92LST/ETtDB61nZPfJdjBp7Ssr4Vs6YDvZKik9zG8SAMGEQhyg58RSR0
xhtTMsRg+Jz2AdC7Ue2jI5JneMk2CMa5FB4vaEm3+KcIE3GwEEFLApzy2IVAZ5Nh
JlzxavNlbQ3+4ZsfakSmS83bpcAt4r/3uRZqErzSiRye8w+QkMOXOjR70hBx8ANV
FeyE39q88OcwbGgXys3/Ci4Le+U0mpymGCz/oHqKAlJmHqsQu0ckBPsMbfsYEMDm
INgMMmNXgSF6p37KxDls2EEXu0A7OtcKJtuYIY/sCM01jO3AYfbbqByYvK/kyjWP
WVff+pHDrG3IYqOZYOoA
=5AR7
-----END PGP SIGNATURE-----
```


### Additional support

**Bitcoin Unlimited** have also voted for this change in [BUIP170](https://forum.bitcoinunlimited.info/t/buip170-implement-chip-2021-01-restrict-transaction-version/79).


## Copyright Notice

Copyright (C)  2021 Tom Zander  
Copyright (C)  2022 Jonathan Silverblood

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

